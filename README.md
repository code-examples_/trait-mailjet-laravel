
#  Trait de Mailjet en Laravel - Ejemplo de implementación

### Introducción

Este Trait de PHP está diseñado específicamente para Laravel y se puede agregar a tu proyecto para permitir el envío de correos electrónicos a través de la APIRest de Mailjet. En general, este Trait es una herramienta útil para ahorrar tiempo de desarrollo y facilitar la integracion de Mailjet en proyectos con Laravel.

**Funciones:**
* ``` getApiKey() ```
* ``` getApiSecret() ```
* ``` getMailFromAddress() ```
* ``` getMailFromName() ```
* ``` initialize() ```
* ``` generateAttachmentFromFilePath( string $filepath ) ```
* ``` generateBody( array $to, string $subject, string $html, array $attachments = [] ) ```
* ``` send( array $body ) ```
    
### Instalación

Pre requisito: tener una cuenta de [Mailjet](https://www.mailjet.com/)

1. Ejecutar: ``` composer require mailjet/mailjet-apiv3-php ```
2. añadir en .env 
    ```
    MJ_APIKEY_PUBLIC='your API key'
    MJ_APIKEY_PRIVATE='your API secret'
    MJ_MAIL_FROM_ADDRESS='test@mailjet.cl'
    MJ_MAIL_FROM_NAME='testing'
    ```
3. Añadir [MJWrapper.php](https://gitlab.com/code-examples_/trait-mailjet-laravel/-/raw/main/app/Traits/MJWrapper.php) en app/Traits
4. Añadir en un [Controller/Service](https://gitlab.com/code-examples_/trait-mailjet-laravel/-/blob/main/app/Http/Controllers/MailjetController.php) el trait [MJWrapper](https://gitlab.com/code-examples_/trait-mailjet-laravel/-/blob/main/app/Traits/MJWrapper.php)
    ```php
    class YourController extends Controller
    {
        use MJWrapper;
    ```

### Ejemplos de uso

```php
    use MJWrapper;
    
    // Constantes con la información del correo electrónico
    const RECIPIENT_EMAIL = 'hola@example.com';
    const RECIPIENT_NAME = 'MJ';
    const SUBJECT = 'Email de prueba desde Mailjet!';
    const ATTACHMENT_FILEPATH = 'invoice.pdf';

    public function testSend()
    {
        try {

            // Crear un array con la información del destinatario
            $to = [
                'email' => self::RECIPIENT_EMAIL,
                'name' => self::RECIPIENT_NAME,
            ];
    
            // Generar el cuerpo del correo electrónico a partir de una vista
            $emailBody = view('email')->render();

            // Generar un array con el adjunto a partir de su ruta
            $attachments[] = $this->generateAttachmentFromFilePath(self::ATTACHMENT_FILEPATH);
        
            // Generar el cuerpo completo del correo electrónico
            $body = $this->generateBody($to, self::SUBJECT, $emailBody, $attachments);
    
            // Enviar el correo electrónico y mostrar un mensaje en pantalla según el resultado
            if ( $this->send($body) ) {
                echo "Email enviado.";
            } else {
                echo "Error al enviar.";
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
```

### Documentaciones oficiales / Referencias
* https://github.com/mailjet/mailjet-apiv3-php
* https://dev.mailjet.com/email/guides/send-api-v31/
