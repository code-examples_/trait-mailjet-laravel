<?php

namespace App\Traits;

use Mailjet\Client;
use \Mailjet\Resources;

trait MJWrapper {

    /**
     * Obtiene la clave pública de la API de Mailjet
     */
    private function getApiKey(): string {
        return getenv('MJ_APIKEY_PUBLIC') ?: '';
    }

    /**
     * Obtiene la clave privada de la API de Mailjet
     */
    private function getApiSecret(): string {
        return getenv('MJ_APIKEY_PRIVATE') ?: '';
    }

    /**
     * Obtiene el correo electrónico del remitente de Mailjet
     */
    private function getMailFromAddress(): string {
        return getenv('MJ_MAIL_FROM_ADDRESS') ?: '';
    }

    /**
     * Obtiene el nombre del remitente de Mailjet
     */
    private function getMailFromName(): string {
        return getenv('MJ_MAIL_FROM_NAME') ?: '';
    }


    /**
     * Inicializa el cliente de Mailjet
     */
    public function initialize(): ?Client {
        try {
            $apiKey = $this->getApiKey();
            $apiSecret = $this->getApiSecret();
     
            if (empty($apiKey) || empty($apiSecret)) {
                throw new \Exception('No se encontraron las claves de API de Mailjet');
            }

            return new Client($apiKey, $apiSecret, true, ['version' => 'v3.1']);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }    

    /**
     * Genera un objeto de adjunto de correo electrónico a partir de un archivo en una ruta determinada
     */
    public function generateAttachmentFromFilePath(string $filePath): ?array {
        try {
            $contentType = mime_content_type($filePath);
            $filename = basename($filePath);
            $base64Content = base64_encode(file_get_contents($filePath));

            return [
                'ContentType' => $contentType,
                'Filename' => $filename,
                'Base64Content' => $base64Content
            ];

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Genera el cuerpo del correo electrónico
     */
    public function generateBody( array $to, string $subject, string $html, array $attachments = [] )
    {
        try {

            $senderEmail = $this->getMailFromAddress();
            $senderName = $this->getMailFromName();

            if (empty($senderEmail) || empty($senderName)) {
                throw new \Exception('No se encontraron los datos del remitente de Mailjet');
            }
                      
            $message = [
                'From' => [
                    'Email' => $senderEmail,
                    'Name' => $senderName
                ],
                'To' => [
                    [
                        'Email' => $to['email'],
                        'Name' => $to['name']
                    ]
                ],
                'Subject' => $subject,
                'HTMLPart' => $html
            ];

            if (!empty($attachments)) {
                foreach ($attachments as $attachment) {
                    $message['Attachments'][] = $attachment;
                }
            }

            return [
                'Messages' => [
                    $message
                ]
            ];

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

     /**
     * Envía el correo electrónico
     */
    public function send(array $body): bool {
        try {
            // Inicializa el cliente de Mailjet
            $mj = $this->initialize();
            if (!$mj) {
                throw new \Exception('No se pudo inicializar el cliente de Mailjet');
            }

            // Envía el correo electrónico
            $response = $mj->post(\Mailjet\Resources::$Email, ['body' => $body]);

            // Retorna el estado del envio correo electrónico
            return $response->success();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

}