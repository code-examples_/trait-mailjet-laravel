<?php

namespace App\Http\Controllers;

use App\Traits\MJWrapper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use stdClass;
use Barryvdh\DomPDF\Facade\Pdf;

class MailjetController extends Controller
{
    use MJWrapper;

    // Constantes con la información del correo electrónico
    const RECIPIENT_EMAIL = 'ifigueroa@aeurus.cl';
    const RECIPIENT_NAME = '';
    const SUBJECT = 'Email de prueba desde Mailjet!';
    const ATTACHMENT_FILEPATH = 'invoice.pdf';

    public function testSend()
    {
        try {

            // Crear un array con la información del destinatario
            $to = [
                'email' => self::RECIPIENT_EMAIL,
                'name' => self::RECIPIENT_NAME,
            ];
    
            // Generar el cuerpo del correo electrónico a partir de una vista
            $emailBody = view('email')->render();

            // Generar un array con el adjunto a partir de su ruta
            $attachments[] = $this->generateAttachmentFromFilePath(self::ATTACHMENT_FILEPATH);
        
            // Generar el cuerpo completo del correo electrónico
            $body = $this->generateBody($to, self::SUBJECT, $emailBody, $attachments);
    
            // Enviar el correo electrónico y mostrar un mensaje en pantalla según el resultado
            if ( $this->send($body) ) {
                echo "Email enviado.";
            } else {
                echo "Error al enviar.";
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
}
